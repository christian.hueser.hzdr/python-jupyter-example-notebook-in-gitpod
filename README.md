# A Python Jupyter Example Notebook in Gitpod

## Introduction

GitLab.com offers a way to deploy an application in Docker containers in a Gitpod development environment.
This example app illustrates how to do that by deploying a Python Jupyter Example Notebook.

## Gitpods on GitLab.com

Gitpods on GitLab.com is a service to create a container development environment in the cloud.
To do so one need to define the environment with a file `.gitpod.yml` in the root folder of your repository.

## Gitpod Definition File

This is the definition file for the Gitpod.
Please read the [Gitpod documentation](https://www.gitpod.io/docs/references/gitpod-yml) to know more about the keywords involved.

```
tasks:
  - name: Run Jupyter Notebook
    command: |
      docker compose up

ports:
  - name: Jupyter notebook port
    port: 8888
    visibility: private
    onOpen: open-preview

vscode:
  extensions:
    - ms-python.python
    - ms-toolsai.jupyter
    - ms-toolsai.jupyter-renderers
    - ms-toolsai.jupyter-keymap
```

### Keyword `image`

Docker container image name to be used as the Docker base image.
If keyword `file` is used as well you can tell Gitpod to build an image based on a file called `.gitpod.Dockerfile`.

### Keyword `tasks`

Tasks are the specifications of how Gitpod prepares and builds your project.
It is recommended to give each task a `name`.
There are three stages in each task of the lists of tasks:
(1) `before`, (2) `init`, (3) `command`,
which will be executed in exact this order.
Steps in (1) `before` are executed each start, and they need to terminate, otherwise the execution stops.
Steps in (2) `init` are executed only once (e.g. at workspace start if workspace has no prebuilt, otherwise it is part of the prebuilt).
Steps in (3) `command` are executed each start as well, but they need not terminate.

### Keyword `ports`

The `ports` keyword configure which ports your application may listen on.
It is recommended to give each `port` a `name`.
With the `visibility` keyword one can declare a port as `private` or `public` (e.g. to share a deployment with your Gitpod team members).
The `onOpen` keyword defines what to do when Gitpod detects a given port is being listened on, i.e. open it in a preview window in the IDE with `open-preview` or in a browser with `open-browser`.

### Keyword `vscode`

With `vscode` in combination with `extensions` one can install Visual Studio Code extensions.

## Run the Gitpod

Go to the GitLab.com project and click on the "Gitpod" button.
If you are using Gitpods the first time you need to enable them by clicking on the WebIDE drop-down arrow, select Gitpod and click on the "Enable Gitpod" button.
After that you can click the "Gitpod" button.
Wait until all is created and a web Visual Studio Code IDE version is launched.
Enjoy the Python Jupyter Example Notebook in a preview window.
